var http = require("http"), fs = require("fs");
var args = process.argv.slice(2);
var url = args[0];
var dest = args[1];

if(dest == undefined) {
	dest = url.split("/");
	fname = parseInt(dest.length) - 1;
	dest = dest[fname];
}
var request = http.get(url, function(res) {
	console.log(url + " > " + dest);
	global.size = (res.headers["content-length"] / 1024).toFixed(1);
	if (global.size < 1024) {
		console.log("Downloading " + global.size + " Kbytes");
	}
	else if (global.size/1024 < 1024){
		console.log("Downloading " + (global.size/1024).toFixed(1) + " Mbytes");
	}
	else {
		console.log("Downloading " + (global.size/(1024*1024)).toFixed(1) + " Gbytes");
	}
	
	global.outstream = fs.createWriteStream(dest);
	global.started = Date.now() / 1024;
	global.st = setInterval(stats, 2000);
}).on('error', function(e) {
	console.log("Got error: " + e.message);
});
global.total = 0;
request.on('response', function(response) {
	response.on('data', function(chunk) {
		global.outstream.write(chunk);
		global.total += chunk.length / 1024;
	});
	response.on('end', function() {
		global.outstream.end();
		global.outstream.destroy();
		clearInterval(global.st);
		console.log("Done in " + process.uptime().toFixed(0) + "s.");
		process.exit();
	});
});
var stats = function() {
	pct = global.total * 100 / global.size;
	speed = global.total / ((Date.now() / 1024) - global.started);
	eta = (global.size-global.total)/speed;
	ueta = "sec";
	if (eta>120){
		eta = eta/60;
		ueta = "min";
	}
	console.log(pct.toFixed(1) + "% at " + speed.toFixed(1) + " KB/s ETA: "+eta.toFixed(1)+ueta);
}
